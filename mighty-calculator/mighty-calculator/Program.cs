using System;
using System.Text.RegularExpressions;

namespace mighty_calculator
{
    class Program
    {
        static void Addition(double a, double b)
        {
            Console.WriteLine(a + b);
        }

        static void Substraction(double a, double b)
        {
            Console.WriteLine(a - b);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World! it's mighty calculator.");
            Console.WriteLine("Enter your equation:");
            Console.WriteLine("Example:\t35 + 26");
            do
            {
                Console.Write(">> ");

                string eq = Console.ReadLine();

                var l = eq.Split(' ');

                if (l.Length == 3)
                {
                    try
                    {
                        switch (l[1])
                        {
                            case "+":
                                Addition(double.Parse(l[0]), double.Parse(l[2]));
                                break;
                            case "-":
                                Substraction(double.Parse(l[0]), double.Parse(l[2]));
                                break;
                            //case "*":
                            //    Multiplication(double.Parse(l[0]), double.Parse(l[2]));
                            //    break;
                            default:
                                Console.WriteLine("Incorrect expression!");
                                break;
                        }
                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine("Incorrect expression!");
                    }
                }
                else
                {
                    Console.WriteLine("Incorrect expression!");
                }
            } while (true);
        }
    }
}
